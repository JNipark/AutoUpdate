﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Configuration;

namespace AutoUpdate
{
    public partial class Form1 : Form
    {
        //static string connectionString = ConfigurationManager.AppSettings["connectionString"].ToString();
        //SqlConnection connection = new SqlConnection(connectionString);
        string currentDir = System.Environment.CurrentDirectory;
        FileInfo fileInfo;
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //connection.Open();

        }

        //private void SqlControl(string filePath)
        //{
        //    fileInfo = new FileInfo(filePath);
        //    try
        //    {
        //        string script = fileInfo.OpenText().ReadToEnd();
        //        SqlCommand sqlCommand = new SqlCommand(script, connection);
        //        int result = sqlCommand.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        string a = ex.ToString();
        //        throw;
        //    }
        //    finally
        //    {
        //        fileInfo = null;
        //    }
        //}

        private string CheckRarFile()
        {

            try
            {
                string[] filesArr = System.IO.Directory.GetFiles(currentDir);
                string[] dirArr = System.IO.Directory.GetDirectories(currentDir);
                progressBar1.Value = 0;
                int provessValue = 50 / filesArr.Length;
                foreach (var item in filesArr)
                {
                    progressBar1.Value += provessValue;
                    if (item.Contains(".rar"))
                    {
                        System.Threading.Thread.Sleep(2000);
                        UnRarOrZip(currentDir, item, true, "");
                        //File.Delete(currentDir + @"\" + item);
                    }
                    System.Threading.Thread.Sleep(50);
                }
                return currentDir;
            }
            catch (Exception ex)
            {
                return currentDir;
            }
        }

        /// <summary>
        /// 解压
        /// </summary>
        /// <param name="unPath"></param>
        /// <param name="rarPathName"></param>
        /// <param name="isCover"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool UnRarOrZip(string unPath, string rarPathName, bool isCover, string password)
        {
            try
            {
                if (!Directory.Exists(unPath))
                {
                    Directory.CreateDirectory(unPath);
                }
                Process process = new Process();
                process.StartInfo.FileName = "Winrar.exe";
                process.StartInfo.CreateNoWindow = true;
                string cmd = "";
                if (!string.IsNullOrEmpty(password) && isCover)
                {
                    cmd = string.Format(" x -p{0} -o+ {1} {2} -y", password, rarPathName, unPath);
                }
                else if (!string.IsNullOrEmpty(password) && !isCover)
                {
                    cmd = string.Format(" x -p{0} -o- {1} {2} -y", password, rarPathName, unPath);
                }
                else if (isCover)
                {
                    cmd = string.Format(" x -o+ {0} {1} -y", rarPathName, unPath);
                }
                else
                {
                    cmd = string.Format(" x -o- {0} {1} -y", rarPathName, unPath);
                }

                process.StartInfo.Arguments = cmd;
                process.Start();
                process.WaitForExit();
                if (process.ExitCode == 0)
                {
                    process.Close();
                    File.Delete(rarPathName);
                    return true;
                }
                else
                {
                    try
                    {
                        File.Delete(rarPathName);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    return false;
                }

            }
            catch (Exception ex)
            {
                string a = ex.ToString();
                return false;
            }
        }

        /// <summary>
        /// 寻找sql文件夹，遍历执行sql文件
        /// </summary>
        public void RunSql()
        {
            string[] dirArr = System.IO.Directory.GetDirectories(currentDir);
            string dirName = "";
            DataBase.Open();
            foreach (var item in dirArr)
            {
                dirName = item.Substring(currentDir.Length);
                if (dirName.Contains("SQL"))
                {
                    string[] filesPath = System.IO.Directory.GetFiles(currentDir + @"\" + dirName);
                    int provessValue = 50 / filesPath.Length;
                    foreach (var file in filesPath)
                    {
                        progressBar1.Value = progressBar1.Value + provessValue;
                       DataBase.SqlControl(file);
                    }

                }
            }
            //System.IO.Directory.Delete(dirName, true);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            CheckRarFile();
            RunSql();
            
            progressBar1.Value = 100;
            System.Threading.Thread.Sleep(1000);
            DataBase.Close();
            System.Threading.Thread.Sleep(1000);
            System.Environment.Exit(0);
        }
    }
}
